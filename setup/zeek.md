# Zeek   

Zeek alias Ubuntu Linux running zeek    

## Prerequisites  
- Connection to VMWare ESXi (see [hypervisor guide](Hypervisor.md))  
- Preferred Linux distribution ISO file (this project uses Ubuntu 18.04)
- Optional: SSH-keypair and SSH client  
- Note: all commands are expected to be run as root or a privileged user

## Install a Ubuntu 18.04 VM  
- Go through the Ubuntu 18.04 guided installation (only install standard system utilities at software selection step, others are not needed)  
- The settings used for this VM are:  
![zeeksettings](pics/zeeksettings.png)  
- Remember to have it connected to the network!  
- Install VMWare Tools   
- Install updates using: apt update && apt upgrade -y  

## Automatic scripted setup
- Prerequisites for install script:  
  	- Gitlab user with SSH public key added  
	- local user on zeekserver  
	- Static IP address in mind, known gateway, netmask  
	- SSH port wishes   
- If you want to use an interactive automated script for setting up the webserver - you can use [this file](https://gitlab.com/its2021modnetgrp3/its_2021_modnet_grp3/-/blob/main/setup/scripts/zeekscript.sh).  
- To download the file to your zeekserver:  
	- apt install -y curl  
	- curl -O https://its2021modnetgrp3.gitlab.io/its_2021_modnet_grp3/setup/scripts/zeekscript.sh  
- To run the script:  
	- chmod +x zeekscript.sh  
	- ./zeekscript.sh  
	- Alternatively, in the exam if your VM has SSH and DHCP, run ./zeekscript.sh exam
	- answer the interactive prompts   
	- reboot machine after the script is finished  
  
## Manual setup  
- For manual setup, follow the instructions below  
- Run the commands as root or privileged user  
- Read and run the commands carefully and correctly  

## Set static ip  
- check your network interfaces using the command "ip a"   
- edit /etc/network/interfaces with your preferred editor (nano, vi, vim, emacs, etc.)  
- add the following lines to the end of the file:  
  ```
  allow-hotplug ens32 ## ens32 is the interface, allow-hotplug starts the interface when a "hotplug" event is detected.
  iface ens32 inet static ## set interface ens32 to have a static ip with details below
      address 10.56.38.30 ## the static ip address for the interface 
      netmask 255.255.255.0 ## netmask
      gateway 10.56.38.1 ## gateway
  ```  
  
## Install, harden/configure, and enable SSH server  
SSH is the best way to securely manage servers remotely.  
- apt install ssh openssh-server #install SSH and SSH server  
- using your preferred text editor (for example nano, vi, vim, emacs, etc.) edit /etc/ssh/sshd_config  
- add the lines   
```'
Port 3333 ## Change default SSH port 22 to 3333 - not for security but to avoid automated bot scans
PasswordAuthentication no ## Disables password authentication for security, SSH key authentication is more secure
PermitRootLogin no ## Disables root login using SSH for security, SSHing into user then su is more secure
```
- place your [public SSH keys](https://docs.gitlab.com/ee/ssh/#generate-an-ssh-key-pair) into /home/bruger/.ssh/authorized_keys  (an easy way to do it is using curl http://gitlab.com/user.keys >> /home/bruger/.ssh/authorized_keys)   
- systemctl enable ssh  #enable ssh server (sshd which means ssh daemon is symlinked to ssh) using systemctl that handles systemd services   
- systemctl start ssh #start ssh server right now using systemctl that handles systemd services  
- access your machine using "ssh bruger@10.56.38.30 -p3333"  
  
## Set firewall rules  
There are two approaches:  
  
### Using Iptables  
- echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc && source /root/.bashrc # add /usr/sbin/ to the PATH environmental variable so the system knows where to execute iptables from  
- iptables -t filter -A INPUT -i lo -j ACCEPT # allow loopback traffic (good practice, it is from your own machine)   
- iptables -t filter -A OUTPUT -o lo -j ACCEPT # allow loopback traffic (good practice, it is from your own machine)   
- iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT ## to make it work with established connections  
- iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT ## to make it work wit hestablishd connections  
- iptables -A INPUT -p tcp --dport 3333 -j ACCEPT # allow port 3333 (our SSH port) incoming traffic  
- iptables -A OUTPUT -p tcp --dport 3333 -j ACCEPT # allow port 3333 (our SSH port) outgoing traffic   
- iptables -A INPUT -p tcp --dport 5601 -j ACCEPT # allow port 5601 (Kibana port) incoming traffic
- iptables -A OUTPUT -p tcp --dport 5601 -j ACCEPT # allow port 5601 (Kibana port) outgoing traffic
- iptables -t filter -P INPUT DROP  # drop input packets   
- iptables -t filter -P FORWARD DROP # drop forward packets    
- iptables -t filter -P OUTPUT DROP # drop output packets  
- iptables-save > /etc/iptables/rules.v4 # save configuration      
- ip6tables-save > /etc/iptables/rules.v6 # save ipv6 configuration   
- to restore rules after reboot, add the following line to your cron (crontab -e):  
```
@reboot /sbin/iptables-restore < /etc/iptables/rules.v4
@reboot /sbin/ip6tables-restore < /etc/iptables/rules.v6

```

### Using UFW (Uncomplicated FireWall - Iptables frontend/wrapper)  
- apt install ufw  #install ufw   
- echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc && source /root/.bashrc # add /usr/sbin/ to the PATH environmental variable so the system knows where to execute ufw from  
- ufw allow 3333  # allow port 3333 (our SSH port) incoming traffic  
- ufw allow 5601  # allow port 5601 (Kibana port) incoming traffic
- sudo ufw default deny incoming # deny incoming connections by default
- ufw enable  # enable ufw iptables frontend firewall

### Install zeek  
- echo 'deb http://download.opensuse.org/repositories/security:/zeek/xUbuntu_18.04/ /' | sudo tee /etc/apt/sources.list.d/security:zeek.list  
- curl -fsSL https://download.opensuse.org/repositories/security:zeek/xUbuntu_18.04/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/security_zeek.gpg > /dev/null
- apt update  
- apt install zeek  
- echo "PATH=$PATH:/opt/zeek/bin/" >> /root/.bashrc && source /root/.bashrc # add /opt/zeek/bin/ to the PATH environmental variable so the system knows where to execute zeekctl from  
- sed -i 's/eth0/ens32/g' /opt/zeek/etc/node.cfg 
- echo "10.56.38.0/24      Private IP space" > /opt/zeek/etc/networks.cfg
- zeekctl
- inside the zeekctl prompt use:
```
install
start
deploy
````
### Install ELK stack (Elasticsearch)

- apt install -y docker.io tmux
- docker pull sebp/elk
- usermod -a -G docker bruger
- systemctl start docker
- systemctl enable docker  
- echo "vm.max_map_count=262144" >> /etc/sysctl.conf  
- sysctl -p  

Open a new termux session with "tmux"  
- docker run -it --rm -p 5601:5601 -p 9200:9200 -p 5044:5044 -v elk-data:/var/lib/elasticsearch sebp/elk /bin/bash
 
#### Configure Elasticsearch
- cd /opt/elasticsearch
- echo "xpack.security.enabled: true" >> /etc/elasticsearch/elasticsearch.yml
- echo "xpack.security.transport.ssl.enabled: true" >> /etc/elasticsearch/elasticsearch.yml
- echo "path.data: /var/lib/elasticsearch" >> /etc/elasticsearch/elasticsearch.yml
- runuser -u elasticsearch -- ./bin/elasticsearch &
- runuser -u elasticsearch -- ./bin/elasticsearch-setup-passwords interactive

##### Configure Kibana
- cd /opt/kibana
- echo "elasticsearch.username: \"elastic\"" >> /opt/kibana/config/kibana.yml
- echo "xpack.encryptedSavedObjects.encryptionKey: \"fhjskloppd678ehkdfdlliverpoolfcr\"" >> /opt/kibana/config/kibana.yml
- chown kibana:kibana /opt/kibana/config/kibana.yml
- runuser -u kibana -- ./bin/kibana-keystore create
- runuser -u kibana -- ./bin/kibana-keystore add elasticsearch.password

#### Stop Elasticsearch process  
- pkill runuser

#### Configure certificate for Elasticsearch
- cd /opt/elasticsearch
- runuser -u elasticsearch -- ./bin/elasticsearch-certutil http
- apt-get update
- apt-get install unzip
- unzip elasticsearch-ssl-http.zip
- mv /opt/elasticsearch/elasticsearch/http.p12 /etc/elasticsearch/http.p12
- echo "xpack.security.http.ssl.enabled: true" >> /etc/elasticsearch/elasticsearch.yml
- echo "xpack.security.http.ssl.keystore.path: \"/etc/elasticsearch/http.p12\"" >> /etc/elasticsearch/elasticsearch.yml
- echo "xpack.security.authc.api_key.enabled: true" >> /etc/elasticsearch/elasticsearch.yml
- chown elasticsearch:elasticsearch /etc/elasticsearch/http.p12

#### Print certificate (print and save it somewhere it is needed later)
- cat /opt/elasticsearch/kibana/elasticsearch-ca.pem

#### Configure certificate for Kibana
- cp /opt/elasticsearch/kibana/elasticsearch-ca.pem /opt/kibana/
- chown kibana:kibana /opt/kibana/elasticsearch-ca.pem
- echo "elasticsearch.ssl.certificateAuthorities: [ \"/opt/kibana/elasticsearch-ca.pem\" ]" >> /opt/kibana/config/kibana.yml
- echo "elasticsearch.hosts: [ \"https://localhost:9200\" ]" >> /opt/kibana/config/kibana.yml
 
#### Configure logstash
- cd /etc/logstash/conf.d
- Put the lines below into 30-output.conf
```
output {
  elasticsearch {
    hosts => [" https://localhost:9200 "]
    cacert => "/opt/kibana/elasticsearch-ca.pem"
    manage_template => false
    index => "%{[@metadata][beat]}-%{[@metadata][version]}-%{+YYYY.MM.dd}"
    user => "elastic"
    password => "Passw0rd"
  }
}
```
Save existing docker container as secure_elk  
- docker commit CONTAINER_ID secure_elk

Kill existing docker container  
- docker kill CONTAINER_ID

Start new secure_elk container (remember to keep elk-data)  
- docker run -it --rm -p 5601:5601 -p 9200:9200 -p 5044:5044 -v elk-data:/var/lib/elasticsearch secure_elk /bin/bash

#### Start the services to be used
- runuser -u elasticsearch -- /opt/elasticsearch/bin/elasticsearch &
- runuser -u kibana -- /opt/kibana/bin/kibana &
- runuser -u logstash -- /opt/logstash/bin/logstash &

Log in on the webpage  
Adress: http://127.0.0.1:5601  
User: elastic  

### Install and configure Filebeat

Run the lines below as root
```
apt install -y gnupg
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
apt-get install -y apt-transport-https
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-7.x.list
apt-get update
apt-get install -y filebeat
systemctl enable filebeat
```
- /etc/filebeat/filebeat.yml has to be edited in multiple ways  
- under output.elasticsearch add hostname, username, password, and https, and under https add CA file location. Example:  
```
output.elasticsearch:
  # Array of hosts to connect to.
  hosts: ["localhost:9200"]

  # Protocol - either `http` (default) or `https`.
  protocol: "https"
  ssl.certificate_authorities: ["/etc/client/ca.pem"] 

  # Authentication credentials - either API key or username/password.
  #api_key: "id:api_key"
  username: "elastic"
  password: "yourpassword"
```
- CA file is from the ELK installation. cat /opt/elasticsearch/kibana/elasticsearch-ca.pem
 
### Connect Zeek and Filebeat

- Prerequisites:  
  	- Zeek installed  
	- Filebeat installed  
- Run the commands below as root
```
apt install -y curl  
curl -O https://its2021modnetgrp3.gitlab.io/its_2021_modnet_grp3/setup/scripts/zeek.yml
filebeat modules enable zeek
mv zeek.yml /etc/filebeat/modules.d/
echo "@load policy/tuning/json-logs.zeek" >> /opt/zeek/share/zeek/site/local.zeek
zeekctl deploy
filebeat setup
systemctl restart filebeat
```

## Start ELK stack on reboot

- Download startelk script to your machine and place it into your users .local/bin directory as startelk and give it execute permissions
```
apt install -y curl  
curl -O https://its2021modnetgrp3.gitlab.io/its_2021_modnet_grp3/setup/scripts/startelk.sh
chmod +700 startelk.sh
mv startelk.sh .local/bin/startelk

```

- As root, add the following line to your crontab using crontab -e

```
@reboot sleep 60 && zeekctl start & su bruger -c "/home/bruger/.local/bin/startelk.sh"
```
