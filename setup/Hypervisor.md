# Hypervisor  

## Prerequisites
- Host PC with a modern operating system installed  
- Connection to the institution's VPN (in the group's case)  

## Installation  
The hypervisor required to connect to the institution's VMWare ESXi is [VMWare Workstation Pro](https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html).

## Connect to the ESXi
- Connect to the institution's VPN  
- Open VMWare Workstation Pro and click "Connect to a Remote Server" on the home menu  
- Enter the ip address and credentials provided by the lecturers  
