#!/bin/sh

tmux new -d -s elkstack 'docker run -it --rm -p 5601:5601 -p 9200:9200 -p 5044:5044 -v elk-data:/var/lib/elasticsearch secure_elk /bin/bash'
tmux send-keys -t elkstack 'runuser -u elasticsearch -- /opt/elasticsearch/bin/elasticsearch &' C-m
sleep 45
tmux send-keys -t elkstack 'runuser -u kibana -- /opt/kibana/bin/kibana &' C-m 
sleep 30
tmux send-keys -t elkstack 'runuser -u logstash -- /opt/logstash/bin/logstash &' C-m
