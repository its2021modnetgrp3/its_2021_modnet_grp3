#!/bin/bash

getvars(){
echo What should the static address be? Example: 10.56.38.30
read IPADDRESS
echo What should the netmask be? Example: 255.255.255.0
read NETMASK
echo What should the gateway be? Example: 10.56.38.1
read GATEWAYIP
echo What should the subnet be? Example: 10.56.38.0/24
read SUBNET
echo What should the SSH port be? Example: 3333
read SSHPORT
echo What should the DNS server be? Example: 10.56.12.11
read DNSSERVER
echo Enter a local username. Example: bruger
read LOCALUSER
echo Enter password for ELK stack. Example: password
read -sp PASSWORD
echo Enter GitLab username. Example: jambove
read GITLABUSER
INTERFACE=$(ip r | grep default | awk '{print $5}')
}
setvars(){
NETMASK=255.255.255.0
GATEWAYIP=$(ip r | grep default | awk '{print $3}')
INTERFACE=$(ip r | grep default | awk '{print $5}')
SUBNET=$(ip a | grep $INTERFACE | awk '{print $2}' | sed -n 2p)
IPADDRESS=$(ip a | grep $INTERFACE | awk '{print $2}' | sed -n 2p | sed 's/...$//')
DNSSERVER=10.56.12.11
SSHPORT=3333
LOCALUSER=bruger
GITLABUSER=jambove
PASSWORD=password
}
setstaticip(){
cat > /etc/network/interfaces.d/staticip-$INTERFACE <<EOF
  allow-hotplug $INTERFACE
  iface $INTERFACE inet static
      address $IPADDRESS 
      netmask $NETMASK
      gateway $GATEWAYIP
EOF
sed 's/.*'$INTERFACE' inet dhcp.*//' /etc/network/interfaces
echo "nameserver $DNSSERVER" > /etc/resolv.conf
}

updupg(){
apt-get update && apt-get upgrade -y
}

sshserver(){
apt install -y ssh openssh-server 
cat >> /etc/ssh/sshd_config<<EOF
Port $SSHPORT
PasswordAuthentication no
PermitRootLogin no
EOF
curl http://gitlab.com/$GITLABUSER.keys >> /home/$LOCALUSER/.ssh/authorized_keys
systemctl enable ssh
systemctl start ssh
}  

firewalliptables(){
echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc
source /root/.bashrc
iptables -t filter -A INPUT -i lo -j ACCEPT
iptables -t filter -A OUTPUT -o lo -j ACCEPT
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp --dport $SSHPORT -j ACCEPT
iptables -A OUTPUT -p tcp --dport $SSHPORT -j ACCEPT
iptables -A INPUT -p tcp --dport 5601 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 5601 -j ACCEPT
iptables -t filter -P INPUT DROP
iptables -t filter -P FORWARD DROP
iptables -t filter -P OUTPUT DROP
iptables-save > /etc/iptables/rules.v4
ip6tables-save > /etc/iptables/rules.v6
cat >> /etc/crontab<<EOF
@reboot /sbin/iptables-restore < /etc/iptables/rules.v4
@reboot /sbin/ip6tables-restore < /etc/iptables/rules.v6
EOF
}

firewallufw(){
apt install -y ufw
echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc
source /root/.bashrc
ufw allow $SSHPORT
ufw allow 80
ufw allow 5601
ufw default deny incoming
ufw enable
}

installzeek(){
apt install -y gnupg
if [[ $(uname -v | grep Debian) ]]; then
echo 'deb https://download.opensuse.org/repositories/security:/zeek/Debian_11/ /' | tee /etc/apt/sources.list.d/security:zeek.list
curl -fsSL https://download.opensuse.org/repositories/security:zeek/Debian_11/Release.key | gpg --dearmor | tee /etc/apt/trusted.gpg.d/security_zeek.gpg > /dev/null
else
echo 'deb http://download.opensuse.org/repositories/security:/zeek/xUbuntu_18.04/ /' | tee /etc/apt/sources.list.d/security:zeek.list  
curl -fsSL https://download.opensuse.org/repositories/security:zeek/xUbuntu_18.04/Release.key | gpg --dearmor | tee /etc/apt/trusted.gpg.d/security_zeek.gpg > /dev/null
fi
apt update  
apt install -y zeek  
echo "PATH=$PATH:/opt/zeek/bin/" >> /root/.bashrc && source /root/.bashrc
echo "PATH=$PATH:/opt/zeek/bin/" >> /home/$LOCALUSER/.bashrc
sed -i 's@eth0@'"$INTERFACE"'@' /opt/zeek/etc/node.cfg
echo "$SUBNET      Private IP space" > /opt/zeek/etc/networks.cfg
zeekctl install
sleep 10
zeekctl start
sleep 10
zeekctl deploy
}


installelk(){
echo "PATH=$PATH:/usr/sbin/" >> /root/.bashrc && source /root/.bashrc
apt install -y docker.io tmux
docker pull sebp/elk
usermod -a -G docker $LOCALUSER
systemctl start docker
systemctl enable docker
echo "vm.max_map_count=262144" >> /etc/sysctl.conf  
sysctl -p  
export PASSWORD=$PASSWORD
su -c 'tmux new -d -s elktmux' $LOCALUSER
su -c '
tmux send-keys -t elktmux "docker run -it --rm -p 5601:5601 -p 9200:9200 -p 5044:5044 -v elk-data:/var/lib/elasticsearch sebp/elk /bin/bash" C-m
tmux send-keys -t elktmux "cd /opt/elasticsearch" C-m
tmux send-keys -t elktmux "
tee <<EOF>> /etc/elasticsearch/elasticsearch.yml
xpack.security.enabled: true
xpack.security.transport.ssl.enabled: true
path.data: /var/lib/elasticsearch
EOF
" C-m
tmux send-keys -t elktmux "runuser -u elasticsearch -- ./bin/elasticsearch &" C-m
sleep 35
tmux send-keys -t elktmux "runuser -u elasticsearch -- ./bin/elasticsearch-setup-passwords interactive" C-m
sleep 2
tmux send-keys -t elktmux "y" C-m
sleep 2
tmux send-keys -t elktmux "$PASSWORD" C-m
sleep 2
tmux send-keys -t elktmux "$PASSWORD" C-m
sleep 2
tmux send-keys -t elktmux "$PASSWORD" C-m
sleep 2
tmux send-keys -t elktmux "$PASSWORD" C-m
sleep 2
tmux send-keys -t elktmux "$PASSWORD" C-m
sleep 2
tmux send-keys -t elktmux "$PASSWORD" C-m
sleep 2
tmux send-keys -t elktmux "$PASSWORD" C-m
sleep 2
tmux send-keys -t elktmux "$PASSWORD" C-m
sleep 2
tmux send-keys -t elktmux "$PASSWORD" C-m
sleep 2
tmux send-keys -t elktmux "$PASSWORD" C-m
sleep 2
tmux send-keys -t elktmux "$PASSWORD" C-m
sleep 2
tmux send-keys -t elktmux "$PASSWORD" C-m
sleep 2
tmux send-keys -t elktmux "cd /opt/kibana" C-m
tmux send-keys -t elktmux "
tee <<EOF>> /opt/kibana/config/kibana.yml
elasticsearch.username: \"elastic\"
xpack.encryptedSavedObjects.encryptionKey: \"fhjskloppd678ehkdfdlliverpoolfcr\"
EOF
" C-m
tmux send-keys -t elktmux "chown kibana:kibana /opt/kibana/config/kibana.yml" C-m
tmux send-keys -t elktmux "runuser -u kibana -- ./bin/kibana-keystore create" C-m
tmux send-keys -t elktmux "runuser -u kibana -- ./bin/kibana-keystore add elasticsearch.password" C-m
sleep 2
tmux send-keys -t elktmux "$PASSWORD" C-m
sleep 2
tmux send-keys -t elktmux "pkill runuser" C-m
sleep 2
tmux send-keys -t elktmux "cd /opt/elasticsearch" C-m
tmux send-keys -t elktmux "runuser -u elasticsearch -- ./bin/elasticsearch-certutil http" C-m
sleep 2
tmux send-keys -t elktmux "n" C-m
sleep 2
tmux send-keys -t elktmux "n" C-m
sleep 2
tmux send-keys -t elktmux "n" C-m
sleep 2
tmux send-keys -t elktmux ENTER
sleep 2
tmux send-keys -t elktmux ENTER
sleep 2
tmux send-keys -t elktmux "n" C-m
sleep 2
tmux send-keys -t elktmux "localhost" C-m
sleep 2
tmux send-keys -t elktmux ENTER
sleep 2
tmux send-keys -t elktmux ENTER
sleep 2
tmux send-keys -t elktmux ENTER
sleep 2
tmux send-keys -t elktmux ENTER
sleep 2
tmux send-keys -t elktmux ENTER
sleep 2
tmux send-keys -t elktmux ENTER
sleep 2
tmux send-keys -t elktmux ENTER
sleep 2
tmux send-keys -t elktmux "apt update && apt install -y unzip" C-m
sleep 20
tmux send-keys -t elktmux "unzip elasticsearch-ssl-http.zip" C-m
tmux send-keys -t elktmux "mv /opt/elasticsearch/elasticsearch/http.p12 /etc/elasticsearch/http.p12" C-m
tmux send-keys -t elktmux "
tee <<EOF>> /etc/elasticsearch/elasticsearch.yml
xpack.security.http.ssl.enabled: true
xpack.security.http.ssl.keystore.path: \"/etc/elasticsearch/http.p12\"
xpack.security.authc.api_key.enabled: true
EOF
" C-m
tmux send-keys -t elktmux "chown elasticsearch:elasticsearch /etc/elasticsearch/http.p12" C-m
tmux send-keys -t elktmux "cp /opt/elasticsearch/kibana/elasticsearch-ca.pem /opt/kibana/" C-m
tmux send-keys -t elktmux "chown kibana:kibana /opt/kibana/elasticsearch-ca.pem" C-m
tmux send-keys -t elktmux "
tee <<EOF>> /opt/kibana/config/kibana.yml
elasticsearch.ssl.certificateAuthorities: [ \"/opt/kibana/elasticsearch-ca.pem\" ]
elasticsearch.hosts: [ \"https://localhost:9200\" ]
EOF
" C-m
tmux send-keys -t elktmux "cd /etc/logstash/conf.d" C-m
tmux send-keys -t elktmux "
tee <<EOF 30-output.conf > /dev/null
output {
  elasticsearch {
    hosts => [\" https://localhost:9200 \"]
    cacert => \"/opt/kibana/elasticsearch-ca.pem\"
    manage_template => false
    index => \"%{[@metadata][beat]}-%{[@metadata][version]}-%{+YYYY.MM.dd}\"
    user => \"elastic\"
    password => \"$PASSWORD\"
  }
}
EOF
" C-m' $LOCALUSER
sleep 10
mkdir /etc/client/
docker cp $(docker container ls | grep sebp/elk | awk '{print $1}'):/opt/elasticsearch/kibana/elasticsearch-ca.pem /etc/client/ca.pem
docker commit $(docker container ls | grep sebp/elk | awk '{print $1}') secure_elk
docker kill $(docker container ls | grep sebp/elk | awk '{print $1}')
su -c 'tmux kill-session -t elktmux' $LOCALUSER
sleep 2
su -c 'tmux new -d -s elkstack' $LOCALUSER
su -c '
tmux send-keys -t elkstack "docker run -it --rm -p 5601:5601 -p 9200:9200 -p 5044:5044 -v elk-data:/var/lib/elasticsearch secure_elk /bin/bash" C-m
sleep 5
tmux send-keys -t elkstack "runuser -u elasticsearch -- /opt/elasticsearch/bin/elasticsearch &" C-m
sleep 20
tmux send-keys -t elkstack "runuser -u kibana -- /opt/kibana/bin/kibana &" C-m
sleep 20
tmux send-keys -t elkstack "runuser -u logstash -- /opt/logstash/bin/logstash &" C-m' $LOCALUSER
}

installfilebeat(){
apt install -y gnupg
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
apt-get install -y apt-transport-https
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-7.x.list
apt-get update
apt-get install -y filebeat
systemctl enable filebeat
cat >> /etc/filebeat/filebeat.yml<<EOF
output.elasticsearch:
  # Array of hosts to connect to.
  hosts: ["localhost:9200"]

  # Protocol - either `http` (default) or `https`.
  protocol: "https"
  ssl.certificate_authorities: ["/etc/client/ca.pem"] 

  # Authentication credentials - either API key or username/password.
  #api_key: "id:api_key"
  username: "elastic"
  password: "$PASSWORD"
EOF
}

zeektoelk(){
apt install -y curl  
curl -O https://its2021modnetgrp3.gitlab.io/its_2021_modnet_grp3/setup/scripts/zeek.yml
filebeat modules enable zeek
mv zeek.yml /etc/filebeat/modules.d/
echo "@load policy/tuning/json-logs.zeek" >> /opt/zeek/share/zeek/site/local.zeek
apt install -y jq
zeekctl deploy
filebeat setup
systemctl restart filebeat
}

generatetraffic(){
dig ucl.dk @8.8.8.8
dig manatee.bar @8.8.8.8
dig jambove.com @8.8.8.8
curl jambove.com
curl icanhazip.com
curl http://httpforever.com/
}

autostart(){
apt install -y curl  
curl -O https://its2021modnetgrp3.gitlab.io/its_2021_modnet_grp3/setup/scripts/startelk.sh
chmod +700 startelk.sh
mkdir -p /home/$LOCALUSER/.local/bin
chown $LOCALUSER:$LOCALUSER /home/$LOCALUSER/.local/bin
mv startelk.sh /home/$LOCALUSER/.local/bin/startelk
chown $LOCALUSER:$LOCALUSER /home/$LOCALUSER/.local/bin/startelk
cat >> /etc/crontab<<EOF
@reboot		root	sleep 60 && /opt/zeek/bin/zeekctl start & su $LOCALUSER -c "/home/$LOCALUSER/.local/bin/startelk"
EOF
}

checkup(){
echo CHECKUPS:
echo " "
echo Static ip:
cat /etc/network/interfaces.d/staticip-$INTERFACE
echo " "
echo SSH server status:
ps -aux | grep "[s]sh"
echo " "
echo Firewall status:
ufw status
}

main(){
getvars
setstaticip
#updupg
sshserver
#firewalliptables
#firewallufw
#installzeek
#installelk
#installfilebeat
#zeektoelk
#autostart
checkup
}

exam(){
INTERFACE=$(ip r | grep default | awk '{print $5}')
SUBNET=$(ip a | grep $INTERFACE | awk '{print $2}' | sed -n 2p)
IPADDRESS=$(ip a | grep $INTERFACE | awk '{print $2}' | sed -n 2p | sed 's/...$//')
LOCALUSER=user
GITLABUSER=jambove
PASSWORD=password
updupg
installzeek
installelk
installfilebeat
zeektoelk
generatetraffic
autostart
}

case "$1" in
	exam)  exam ;;
	start)	main ;;
	help) echo "Available parameters: start, exam, help" ;;
	*)	main ;;

esac
