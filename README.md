# Repository for IT Security education 2021 Modnet Project Group 3  

## Group members:  
- Levente Atilla Taner  
- Marc Rasmus Jensen  
- Martin Hansson  
- Thomas Simon     

## Overview
The aim of this project is to create a virtualized environment consisting of several devices with different operating systems, emulating a real world scenario to better understand how similar systems are set up from scratch, and how they work. This will provide experience of working with systems with security in mind and an environment for future tests.  


## System to be built:
<table>
	<tr>
		<th>Device name</th>
		<th>Description</th>
		<th>OS</th>
		<th>Disk</th>
		<th>RAM</th>
		<th>CPU</th>
	</tr>
	<tr>
		<td>[zeek](setup/zeek.md)</td>
		<td>Zeek</td>
		<td>Ubuntu 18.04</td>
		<td>20 GB</td>
		<td>8 GB</td>
		<td>2</td>
	</tr>
</table>

This environment will be running on a server running VMWare ESXi.   
Server details: 3 Dell servers with 96 GB RAM 2 Intel (R) Xeon (R) Silver with 8 cores in each, 4 network cards 1 GB and 1 storage connectors which are connected to storage controllers A and B via 12GB connection to a common direct access storage which makes vmotion possible.

## IP addresses  
The group has been assigned VLAN106 and the IP range 10.56.38.30-39/24  

## High Level Diagram  

![diagram](diagram.png)



